/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.reward;

import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.reward.commands.RewardListCommand;
import fr.qilat.oxylia.reward.commands.RewardReloadCommand;
import fr.qilat.oxylia.reward.commands.RewardTimeCommand;
import fr.qilat.oxylia.reward.data.Reward;
import fr.qilat.oxylia.reward.data.RewardData;
import fr.qilat.oxylia.reward.inventory.RewardInventory;
import fr.qilat.oxylia.reward.listener.RewardInventoryListener;
import fr.qilat.oxylia.user.data.User;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class RewardManager {

    private RewardData data;
    @Getter
    private HashMap<Integer, Reward> rewards;
    @Getter
    private HashMap<UUID, RewardInventory> openedInventories;


    public RewardManager() {
        this.data = new RewardData();
        this.rewards = new HashMap<>();
        this.openedInventories = new HashMap<>();
    }

    public static final int A_DAY_IN_MILLIS = 24 * 60 * 60 * 1000;

    /**
     * Register reward manager's listener
     */
    public void registerListeners() {
        Oxylia.getInstance().getServer().getPluginManager().registerEvents(new RewardInventoryListener(), Oxylia.getInstance());
    }

    /**
     * Load rewards into map. Must be use only after SQLManager has init sql connection to database.
     */
    public void loadRewards() {
        this.rewards = this.data.getRewards();
    }

    /**
     * Register and open a reward inventory for a specific player
     *
     * @param player targeted player
     */
    public void openRewardInventory(Player player) {
        RewardInventory ri = new RewardInventory(Oxylia.getUserManager().getUsers().get(player.getUniqueId()));
        this.getOpenedInventories().put(player.getUniqueId(), ri);
        player.openInventory(ri.getInventory());
    }

    /**
     * Destroy a reward inventory for a specific player
     * @param player targeted player
     */
    public void closeRewardInventory(Player player) {
        this.getOpenedInventories().remove(player.getUniqueId());
    }

    /**
     * Register all commands of reward handling
     */
    public void registerCommands() {
        Oxylia.getInstance().getServer().getPluginCommand("rewardsreload").setExecutor(new RewardReloadCommand());
        Oxylia.getInstance().getServer().getPluginCommand("rewardslist").setExecutor(new RewardListCommand());
        Oxylia.getInstance().getServer().getPluginCommand("rewardstime").setExecutor(new RewardTimeCommand());
    }

    /**
     * Check if suffisant amount of time has passed since the user take back his last reward
     * @param user checked user
     * @return true if next award available
     */
    public boolean isNextRewardAvailableOnTime(User user) {
        long elapsedTime = System.currentTimeMillis() - user.getLastRewardTime();
        return elapsedTime >= A_DAY_IN_MILLIS;
    }

    /**
     * Return reward by using its day id
     * @param dayId day id of the reward
     * @return reward obejct
     */
    public Reward getRewardByDayId(int dayId) {
        for (Reward reward : this.getRewards().values())
            if (reward.getDayId() == dayId)
                return reward;
        return null;
    }
}
