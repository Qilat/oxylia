package fr.qilat.oxylia.sql;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SQLDriver {

    MYSQL("com.mysql.jdbc.Driver", "mysql"),
    POSTGRESQL("org.postgresql.Driver", "postgresql"),
    MARIADB("org.mariadb.jdbc.Driver", "mariadb");

    private String className;
    private String urlName;
}
