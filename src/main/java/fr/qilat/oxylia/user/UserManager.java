/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.user;

import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.user.data.User;
import fr.qilat.oxylia.user.data.UserData;
import fr.qilat.oxylia.user.listener.ConnectionListener;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.UUID;

public class UserManager {

    @Getter
    private UserData data;
    @Getter
    private HashMap<UUID, User> users;

    public UserManager(){
        this.data = new UserData();
        this.users = new HashMap<>();
    }

    /**
     * Create an load Oxylia user from bukkit player.
     *
     * @param player bukkit player
     */
    public void createUser(Player player) {
        new BukkitRunnable() {
            @Override
            public void run() {
                User user = UserManager.this.data.loadUser(player);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        UserManager.this.users.put(user.getUuid(), user);
                    }
                }.runTask(Oxylia.getInstance());
            }
        }.runTaskAsynchronously(Oxylia.getInstance());
    }

    /**
     * Destroy and save a user.
     * @param uuid uuid of the user to save.
     */
    public void destroyUser(UUID uuid) {
        User user = this.users.get(uuid);
        new BukkitRunnable() {
            @Override
            public void run() {
                UserManager.this.data.saveUser(user);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        UserManager.this.getUsers().remove(uuid);
                    }
                }.runTask(Oxylia.getInstance());
            }
        }.runTaskAsynchronously(Oxylia.getInstance());
    }

    /**
     * Register listeners of user manager
     */
    public void registerListeners() {
        Oxylia.getInstance().getServer().getPluginManager().registerEvents(new ConnectionListener(), Oxylia.getInstance());
    }

}
