package fr.qilat.oxylia.reward.commands;

import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RewardReloadCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player
                && !commandSender.isOp()) {
           Utils.sendNoPermIG(commandSender);
            return false;
        }

        Utils.sendInfoIG(commandSender, "Mise à jour des récompenses...");
        Oxylia.getRewardManager().loadRewards();
        Utils.sendInfoIG(commandSender, "Récompenses mises à jour...");
        return true;
    }
}
