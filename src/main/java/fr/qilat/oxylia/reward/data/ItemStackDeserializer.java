/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.reward.data;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Type;
import java.util.List;

public class ItemStackDeserializer implements JsonDeserializer<ItemStack> {

    /**
     * Déserialize an itemstack from json
     *
     * @param jsonElement                json element
     * @param type                       useless param
     * @param jsonDeserializationContext useless param
     * @return an itemstack
     * @throws JsonParseException error
     */
    @Override
    public ItemStack deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject parent = jsonElement.getAsJsonObject();

        Material material = Material.getMaterial(getStringValue(parent, "material", "STONE"));
        int amount = getIntValue(parent, "amount", 1);
        String displayName = getStringValue(parent, "displayName", null);

        JsonElement loreJS = parent.get("lore");
        List<String> lore = null;
        if(loreJS != null) {
            lore = new Gson().fromJson(loreJS, new TypeToken<List<String>>() {}.getType());
        }

        ItemStack is = new ItemStack(material, amount);
        ItemMeta meta = is.getItemMeta();
        if(displayName != null)
            meta.setDisplayName(displayName);
        if(lore != null && lore.size() > 0)
            meta.setLore(lore);

        return is;
    }

    /**
     * Shortcut method to get a string value or default
     * @param parent json parent object
     * @param identifier object identifier
     * @param defaultValue default value
     * @return a string
     */
    private String getStringValue(JsonObject parent, String identifier, String defaultValue){
        JsonElement valueJS = parent.get(identifier);
        String value = defaultValue;
        if(valueJS != null)
            value = valueJS.getAsString();
        return value;
    }

    /**
     * Shortcut method to get an int value or default
     * @param parent json parent object
     * @param identifier object identifier
     * @param defaultValue default value
     * @return an int
     */
    private int getIntValue(JsonObject parent, String identifier, int defaultValue){
        JsonElement valueJS = parent.get(identifier);
        int value = defaultValue;
        if(valueJS != null)
            value = valueJS.getAsInt();
        return value;
    }

}
