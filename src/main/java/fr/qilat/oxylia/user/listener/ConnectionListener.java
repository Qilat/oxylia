/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.user.listener;

import fr.qilat.oxylia.Oxylia;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player p = event.getPlayer();
        if(!Oxylia.getUserManager().getUsers().containsKey(event.getPlayer().getUniqueId())){
            Oxylia.getUserManager().createUser(p);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        if(Oxylia.getUserManager().getUsers().containsKey(event.getPlayer().getUniqueId())){
            Oxylia.getUserManager().destroyUser(p.getUniqueId());
        }
    }

}
