/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.user.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class User {

    private UUID uuid;
    private Player player;
    @Setter
    private int lastRewardId;
    @Setter
    private long lastRewardTime;

}
