/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.sql;

import fr.qilat.oxylia.Utils;
import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class SQLManager {

    private BasicDataSource connectionPool;

    public SQLManager(SQLConnectInfos infos) {
        try {
            Class.forName(infos.getDriver().getClassName());
            this.connectionPool = createConnectionPool(infos);
        } catch (ClassNotFoundException e) {
            Utils.severe("Driver can't be load.");
            e.printStackTrace();
        }

    }

    /**
     * Create a sql connection pool
     * @param infos sql connection infos
     * @return connection pool
     */
    private static BasicDataSource createConnectionPool(SQLConnectInfos infos) {
        BasicDataSource connectionPool = new BasicDataSource();
        connectionPool.setDriverClassName(infos.getDriver().getClassName());
        connectionPool.setUsername(infos.getUser());
        connectionPool.setPassword(infos.getPassword());
        connectionPool.setUrl(infos.getConnectionURL());
        connectionPool.setInitialSize(infos.getInitialPoolSize());
        connectionPool.setMaxTotal(infos.getMaxPoolSize());
        return connectionPool;
    }

    /**
     * Get a connection from the pool
     * @return connection
     * @throws SQLException thrown by sql lib
     */
    private Connection getConnection() throws SQLException {
        return connectionPool.getConnection();
    }

    /**
     * Make a query and return a ResultSet.
     * @param query query to execute
     * @return ResultSet
     */
    public ResultSet select(String query, HashMap<Integer, String> params) {
        try {
            Connection c = getConnection();
            PreparedStatement state = c.prepareStatement(query);
            params.forEach((parameterIndex, x) -> {
                try {
                    state.setString(parameterIndex, x);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            return state.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Make a query.
     *
     * @param query query to execute
     */
    public void execute(String query, HashMap<Integer, String> params) {
        try {
            Connection c = getConnection();
            PreparedStatement state = c.prepareStatement(query);
            params.forEach((parameterIndex, x) -> {
                try {
                    state.setString(parameterIndex, x);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            state.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
