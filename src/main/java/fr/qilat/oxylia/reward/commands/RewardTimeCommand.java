/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.reward.commands;

import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.Utils;
import fr.qilat.oxylia.reward.RewardManager;
import fr.qilat.oxylia.reward.data.Reward;
import fr.qilat.oxylia.user.data.UUIDConverter;
import fr.qilat.oxylia.user.data.User;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class RewardTimeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length > 0) {
            String pseudo = strings[0];
            Player target = Bukkit.getPlayer(pseudo);
            if (target == null) {
                String strUuid = UUIDConverter.getUUIDFromName(pseudo, true, true);
                if (strUuid != null) {
                    UUID uuid = UUID.fromString(strUuid);
                    if (Oxylia.getUserManager().getData().userExist(uuid)) {
                        User user = Oxylia.getUserManager().getData().loadOfflineUser(uuid);
                        showOnePlayerAnswer(user, commandSender);
                    } else {
                        Utils.sendSevereIG(commandSender, "Joueur introuvable");
                    }
                } else {
                    Utils.sendSevereIG(commandSender, "Joueur introuvable");
                }
            } else {
                User user = Oxylia.getUserManager().getUsers().get(target.getUniqueId());
                showOnePlayerAnswer(user, commandSender);
            }

        } else {
            if (commandSender instanceof ConsoleCommandSender) {
                Utils.sendNoPermIG(commandSender);
                return false;
            }
            User target = Oxylia.getUserManager().getUsers().get(((Player) commandSender).getUniqueId());
            showOnePlayerAnswer(target, commandSender);
        }
        return true;
    }

    private void showOnePlayerAnswer(User target, CommandSender toShow) {
        if (Oxylia.getRewardManager().isNextRewardAvailableOnTime(target)) {
            Utils.sendInfoIG(toShow, "La récompense est déjà disponible !");

            Reward reward = target.getLastRewardId() == -1 ? Oxylia.getRewardManager().getRewardByDayId(0) : Oxylia.getRewardManager().getRewardByDayId(Oxylia.getRewardManager().getRewards().get(target.getLastRewardId()).getDayId() + 1);
            final String[] msg = {"§aRécompense :"};
            reward.getItems().forEach(itemStack -> {
                msg[0] += " Item : " + itemStack.getType() + ". Montant : " + itemStack.getAmount() + ".";
            });
            toShow.sendMessage(msg[0]);

        } else {
            long remainingSeconds = ((target.getLastRewardTime() + RewardManager.A_DAY_IN_MILLIS) - System.currentTimeMillis()) / 1000;
            long hours = remainingSeconds / 3600;
            long minutes = (remainingSeconds - (hours * 3600)) / 60;
            long seconds = (remainingSeconds - (hours * 3600) - (minutes * 60));
            String remainingTime = (hours >= 10 ? hours : ("0" + hours))
                    + ":" + (minutes >= 10 ? minutes : ("0" + minutes))
                    + ":" + (seconds >= 10 ? seconds : ("0" + seconds));
            Utils.sendInfoIG(toShow, "Temps restant avant la prochaine récompense : " + remainingTime);
        }
    }
}
