/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia;

import fr.qilat.oxylia.reward.RewardManager;
import fr.qilat.oxylia.sql.SQLConnectInfos;
import fr.qilat.oxylia.sql.SQLDriver;
import fr.qilat.oxylia.sql.SQLManager;
import fr.qilat.oxylia.user.UserManager;
import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Oxylia extends JavaPlugin {

    @Getter
    private static Oxylia instance;
    @Getter
    private static SQLManager sqlManager;
    @Getter
    private static RewardManager rewardManager;
    @Getter
    private static UserManager userManager;


    @Override
    public void onEnable() {
        Oxylia.instance = this;

        this.saveDefaultConfig();

        FileConfiguration config = this.getConfig();
        SQLDriver driver = null;
        try {
             driver = SQLDriver.valueOf(config.getString("sql.driver"));
        }catch (IllegalArgumentException e){
            Utils.severe("ERROR: driver in config is not recognized.");
            e.printStackTrace();
            this.getServer().shutdown();
        }

        SQLConnectInfos sqlInfos = new SQLConnectInfos(
                driver,
                config.getString("sql.host", "localhost"),
                config.getInt("sql.port", 5432),
                config.getString("sql.database", "oxylia"),
                config.getString("sql.user", "oxylia"),
                config.getString("sql.password", "password"),
                config.getBoolean("sql.autoReconnect", true),
                config.getInt("sql.initialPoolSize", 10),
                config.getInt("sql.maxPoolSize", 20)
        );

        Oxylia.sqlManager = new SQLManager(sqlInfos);

        Oxylia.rewardManager = new RewardManager();
        Oxylia.getRewardManager().loadRewards();
        Oxylia.getRewardManager().registerCommands();
        Oxylia.getRewardManager().registerListeners();

        Oxylia.userManager = new UserManager();
        Oxylia.getUserManager().registerListeners();
    }

    @Override
    public void onDisable() {

    }
}
