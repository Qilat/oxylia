/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.reward.data;

import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.user.data.User;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Reward {
    @Getter
    private int id;
    @Getter
    private int dayId;
    @Getter
    private Material viewItem;
    @Getter
    @Setter
    private List<ItemStack> items;

    public Reward(int id, int dayId, Material viewItem, List<ItemStack> items){
        this.id = id;
        this.dayId = dayId;
        this.viewItem = viewItem;
        this.items = items;
    }

    /**
     * Get the reward itemstack shown into the reward inventory
     *
     * @param user            user who will see this item.
     * @param lastRewardDayId last reward day id to get next day id
     * @return an itemstack to be shown into reward inventory
     */
    public ItemStack getRewardInventoryItem(User user, int lastRewardDayId) {
        ItemStack itemStack = new ItemStack(this.viewItem, 1);
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName("Jour n°" + this.dayId);

        ArrayList<String> lore = new ArrayList<>();
        if ((lastRewardDayId + 1) == this.dayId) {
            if(Oxylia.getRewardManager().isNextRewardAvailableOnTime(user)){
                lore.add("§aRécompense disponible");
            } else {
                lore.add("§aRécompense bientôt disponible");
            }
        } else if ((lastRewardDayId + 1) < this.dayId) {
            lore.add("§cRécompense bloquée");
        } else {
            lore.add("§7Récompense récupérée");
        }
        meta.setLore(lore);
        itemStack.setItemMeta(meta);

        return itemStack;
    }

}
