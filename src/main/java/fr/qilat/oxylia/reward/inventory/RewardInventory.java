/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.reward.inventory;

import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.reward.data.Reward;
import fr.qilat.oxylia.user.data.User;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class RewardInventory {
    public static String TITLE = "Liste des récompenses";
    private static int SIZE = 6*9;

    @Getter
    private User user;
    @Getter
    private Inventory inventory;
    @Getter
    private HashMap<Integer, Reward> rewardLink;

    public RewardInventory(User user){
        this.user = user;
        this.inventory = Bukkit.createInventory(user.getPlayer(), SIZE, TITLE);
        this.rewardLink = new HashMap<>();
        this.refresh();
    }

    /**
     * Called to refresh the reward inventory when a player take back a reward
     */
    public void refresh() {
        this.rewardLink.clear();
        List<Reward> list = new ArrayList<>(Oxylia.getRewardManager().getRewards().values());
        list.sort(Comparator.comparingInt(Reward::getDayId));

        int lastRewardDayId = list.stream().filter((reward) -> (reward.getId() == user.getLastRewardId())).map(Reward::getDayId).findFirst().orElse(-1);

        for (int i = 0; i < SIZE && i < list.size(); i++) {
            this.inventory.setItem(i, list.get(i).getRewardInventoryItem(user, lastRewardDayId));
            this.rewardLink.put(i, list.get(i));
        }
    }



}
