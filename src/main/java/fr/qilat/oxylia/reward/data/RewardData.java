/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.reward.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.Utils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RewardData {

    private GsonBuilder builder;

    public RewardData() {
        this.builder = new GsonBuilder();
        this.builder.registerTypeAdapter(ItemStack.class, new ItemStackDeserializer());
    }

    /**
     * Load all rewards from the database
     *
     * @return an hashmap of the rewards ordered by their id and not by their day id
     */
    public HashMap<Integer, Reward> getRewards() {
        HashMap<Integer, Reward> rewards = new HashMap<>();

        assert Oxylia.getSqlManager() != null;
        ResultSet rs = Oxylia.getSqlManager().select("SELECT * FROM rewards;", new HashMap<>());
        try {
            while(rs.next()) {
                int id = rs.getInt("id");
                int dayId = rs.getInt("day_id");
                Material viewItem = Material.getMaterial(rs.getString("view_item"));

                String json = rs.getString("json_items");
                Gson gson = this.builder.create();
                List<ItemStack> items = gson.fromJson(json, new TypeToken<ArrayList<ItemStack>>(){}.getType());

                rewards.put(id, new Reward(id, dayId, viewItem, items));
            }
        } catch (SQLException e){
            Utils.warning("Error while getting rewards.");
            e.printStackTrace();
        }
        return rewards;
    }
}
