package fr.qilat.oxylia.reward.commands;

import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RewardListCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Utils.sendInfoIG(commandSender, "Listes des récompenses : ");

        Oxylia.getRewardManager().getRewards().forEach((id, reward) -> {

            final String[] msg = {"§aJour n°" + id + " : "};

            reward.getItems().forEach(itemStack -> {
                msg[0] += " Type : " + itemStack.getType() + ". Montant : " + itemStack.getAmount() + ".";
            });

            commandSender.sendMessage(msg[0]);
        });

        if(commandSender instanceof Player) {
            Oxylia.getRewardManager().openRewardInventory((Player)commandSender);
        }

        return true;
    }
}
