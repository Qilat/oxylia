/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia;

import org.bukkit.command.CommandSender;

public class Utils {

    /**
     * Shortcut method to send info-level message into console
     *
     * @param string message to send
     */
    public static void info(String string){
        Oxylia.getInstance().getLogger().info(string);
    }

    /**
     * Shortcut method to send severe-level message into console
     * @param string message to send
     */
    public static void severe(String string){
        Oxylia.getInstance().getLogger().severe(string);
    }

    /**
     * Shortcut method to send warning-level message into console
     * @param string message to send
     */
    public static void warning(String string){
        Oxylia.getInstance().getLogger().warning(string);
    }


    private static final String PREFIX = "§3[Oxylia] ";


    private static void sendMessage(CommandSender commandSender, String msg) {
        commandSender.sendMessage(PREFIX + msg);
    }

    /**
     * Shortcut method to uniformize all output message of this plugin
     * @param commandSender who will see the message
     * @param msg message to send
     */
    public static void sendInfoIG(CommandSender commandSender, String msg){
        Utils.sendMessage(commandSender, "§a" + msg);
    }

    /**
     * Shortcut method to uniformize all output message of this plugin
     * @param commandSender who will see the message
     * @param msg message to send
     */
    public static void sendWarningIG(CommandSender commandSender, String msg){
        Utils.sendMessage(commandSender, "§e" + msg);
    }

    /**
     * Shortcut method to uniformize all output message of this plugin
     * @param commandSender who will see the message
     * @param msg message to send
     */
    public static void sendSevereIG(CommandSender commandSender, String msg){
        Utils.sendMessage(commandSender, "§c" + msg);
    }

    /**
     * Shortcut method to uniformize all output message of this plugin
     * @param commandSender who will see the message
     */
    public static void sendNoPermIG(CommandSender commandSender){
        Utils.sendSevereIG(commandSender, "Vous n'avez pas la permission d'effectuer cela.");
    }

}
