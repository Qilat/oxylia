package fr.qilat.oxylia.sql;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Getter
public class SQLConnectInfos {

    @Getter
    private SQLDriver driver;
    @Getter
    private String host;
    @Getter
    private int port;
    @Getter
    private String database;
    @Getter
    private String user;
    @Getter
    private String password;
    @Getter
    private boolean autoReconnect;
    @Getter
    private int initialPoolSize;
    @Getter
    private int maxPoolSize;

    /**
     * Get all sql connection infos summarized into sql connection string
     * @return sql connection string required by jdbc/mysql-connector
     */
    public String getConnectionURL() {
        String url = "jdbc" + ":" + driver.getUrlName() + "://" + host + ":" + port + "/" + database;
        if (autoReconnect)
            url += "?autoReconnect=true";
        return url;
    }
}
