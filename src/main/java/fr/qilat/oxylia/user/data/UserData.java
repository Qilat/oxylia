/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.user.data;

import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.Utils;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class UserData {

    /**
     * Check if a user existing by checking his uuid
     *
     * @param uuid uuid of the player
     * @return true if user exist in database.
     */
    public boolean userExist(UUID uuid) {
        HashMap<Integer, String> params = new HashMap<>();
        params.put(1, uuid.toString());
        ResultSet rs = Oxylia.getSqlManager().select("SELECT id FROM users WHERE uuid = ?", params);
        try {
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Load an offline player. Without bukkit player object
     * @param uuid uuid of the player.
     * @return User instance with a null player
     */
    public User loadOfflineUser(UUID uuid) {
        HashMap<Integer, String> params = new HashMap<>();
        params.put(1, uuid.toString());
        ResultSet rs = Oxylia.getSqlManager().select("SELECT * FROM users WHERE uuid = ?;", params);

        try {
            if (rs.next()) {
                return new User(
                        uuid,
                        null,
                        rs.getInt("last_reward_id"),
                        rs.getLong("last_reward_time")
                );
            }
        } catch (SQLException e) {
            Utils.severe("Unable to load player with uuid : " + uuid);
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Load online player on connection
     * @param player bukkit player
     * @return Oxylia player
     */
    public User loadUser(Player player){
        HashMap<Integer, String> params = new HashMap<>();
        params.put(1, player.getUniqueId().toString());
        ResultSet rs = Oxylia.getSqlManager().select("SELECT * FROM users WHERE uuid = ?;", params);

        try {
            if (!rs.next()) {
                User user = new User(player.getUniqueId(), player, -1, -1);
                Oxylia.getSqlManager().execute("INSERT INTO users (uuid) VALUES (?)", params);
                return user;
            }else {
                return new User(
                        player.getUniqueId(),
                        player,
                        rs.getInt("last_reward_id"),
                        rs.getLong("last_reward_time")
                );
            }
        } catch (SQLException e) {
            Utils.severe("Unable to load player with uuid : " + player.getUniqueId());
            e.printStackTrace();
            new BukkitRunnable() {
                @Override
                public void run() {
                    player.kickPlayer("Impossible de charger vos données, veuillez contacter un membre du staff");
                }
            }.runTask(Oxylia.getInstance());
        }
        return null;
    }

    /**
     * Save all user data into the database
     * @param user user to save
     */
    public void saveUser(User user){
        Oxylia.getSqlManager().execute("UPDATE users SET last_reward_id = " + user.getLastRewardId() + ", last_reward_time = " + user.getLastRewardTime() + " WHERE uuid = " + user.getUuid(), new HashMap<>());
    }

}
