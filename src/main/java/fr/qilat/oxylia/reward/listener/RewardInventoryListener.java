/*
 * Copyright Qilat (c) 2019.
 * Made for Oxylia.
 */

package fr.qilat.oxylia.reward.listener;

import fr.qilat.oxylia.Oxylia;
import fr.qilat.oxylia.reward.data.Reward;
import fr.qilat.oxylia.reward.inventory.RewardInventory;
import fr.qilat.oxylia.user.data.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

public class RewardInventoryListener implements Listener {

    /**
     * Give the reward to the player if all conditions are good
     *
     * @param event inventory event
     */
    @EventHandler
    public void onClickInventory(InventoryClickEvent event) {
        if (event.getClickedInventory() != null
                && event.getClickedInventory().getName().equals(RewardInventory.TITLE)
                && event.getWhoClicked() instanceof Player) {
            event.setCancelled(true);
            int slotId = event.getSlot();

            RewardInventory ri = Oxylia.getRewardManager().getOpenedInventories().get(event.getWhoClicked().getUniqueId());
            Reward reward = ri.getRewardLink().get(slotId);
            User user = Oxylia.getUserManager().getUsers().get(event.getWhoClicked().getUniqueId());

            if (reward != null
                    && ((user.getLastRewardId() == -1 && reward.getDayId() == 0)
                    || reward.getDayId() == (Oxylia.getRewardManager().getRewardByDayId(user.getLastRewardId()).getDayId() + 1))
                    && Oxylia.getRewardManager().isNextRewardAvailableOnTime(user)) {
                user.getPlayer().getInventory().addItem(reward.getItems().toArray(new ItemStack[0]));
                user.setLastRewardTime(System.currentTimeMillis());
                user.setLastRewardId(reward.getId());
                ri.refresh();
            }
        }
    }

    /**
     * Destroy reward inventory when player quit the inventory
     *
     * @param event inventory close event
     */
    @EventHandler
    public void onQuitInventory(InventoryCloseEvent event) {
        if (event.getInventory() != null
                && event.getInventory().getName().equals(RewardInventory.TITLE)
                && event.getPlayer() instanceof Player) {
            Oxylia.getRewardManager().closeRewardInventory((Player) event.getPlayer());
        }
    }

}
